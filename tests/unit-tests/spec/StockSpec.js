/*global define */
/*global beforeEach, afterEach */
/*global describe, it, expect */
/*global window, eb, loadFixtures, expectedValue */
/*global app */
/*jslint browser: true, plusplus:true*/
/*jslint vars: true, devel: true, nomen: true, indent: 4, maxerr: 50 */

describe("html tests", function () {
    "use strict";
    var properties, methods;

    // Initialize test environment
    beforeEach(function () {
        var propertyName;

        window.app.init();

        properties = [];
        methods = [];
        for (propertyName in window.app) {
            if (window.app.hasOwnProperty(propertyName)) {
                if (app[propertyName].constructor === Function) {
                    methods.push(app[propertyName].prototype);
                } else {
                    properties.push(propertyName);
                }
            }
        }
    });

    afterEach(function () {
        document.body.removeChild(document.querySelector("#container"));
    });

    it("Should verify that the app has properties and methods.", function () {
        expect(properties.length).not.toBe(0);
        expect(methods.length).not.toBe(0);

    });

    it("Should verify that DOM elements are created for the container and the title", function () {
        var expectedValue = 'Real Time Stockquote App', actualValue = window.app.initHTML().querySelector("h1").innerText;

        expect(actualValue).toBe(expectedValue);
    });

    it("Should verify that the table has 26 rows", function () {
        var actualValue = window.app.showData().querySelectorAll("tr").length;
        expect(actualValue).toBe(26);
    });


});


describe("method tests", function () {
    "use strict";
    it("Should verify that only letters and numbers remain", function () {
        var actualValue = window.app.createValidCSSNameFromCompany("HGF>&^#%%$#@"), expectedValue = "HGF";
        expect(actualValue).toBe(expectedValue);
    });

    it("Should verify that series is defined and is an object", function () {
        var actualValue = window.app.series;
        expect(actualValue).toBeDefined();
        expect(actualValue.constructor).toBe(Object);
    });

    it("Should verify retrieving realtime data cannot be tested", function () {
        var actualValue = window.app.getRealtimeData(), expectedValue = undefined;
        expect(actualValue).toBe(expectedValue);
    });

    it("Should verify that ranges including min and max values are returned", function () {
        var input = 100, range = 5, hitMin = 0, hitMax = 0, i, r;

        for (i = 0; i < 1000; i++) {
            r = window.app.rnd(input, range);
            if (r === input - range) {
                hitMin++;
            } else if (r === input + range) {
                hitMax++;
            }
        }

        console.log('hitMin', hitMin);
        console.log('hitMax', hitMax);

        expect(hitMin).toBeGreaterThan(0);
        expect(hitMax).toBeGreaterThan(0);
    });

    it("Should verify that an appropriate date format is returned", function () {
        var date = new Date(), day, month, year;

        date = window.app.getFormattedDate(date);

        month = date.substr(0, 2);
        day = date.substr(3, 5);
        year = date.substr(6, 10);

        expect(parseInt(month, 10)).toBeGreaterThan(0);
        expect(parseInt(month, 10)).toBeLessThan(13);
        expect(parseInt(day, 10)).toBeGreaterThan(0);
        expect(parseInt(day, 10)).toBeLessThan(32);
        expect(parseInt(year, 10)).toBeGreaterThan(0);
        expect(parseInt(year, 10)).toBeLessThan(3000);
    });


    it("Should verify that new quote is added to series", function () {
        var quote = {}, date = new Date(), quotes = [], quoteCount = 0;

        window.app.series = {};

        quote.col0 = "some company";
        quote.col1 = window.app.rnd(0, 100);
        quote.col2 = window.app.getFormattedDate(date);
        quote.col3 = window.app.getFormattedTime(date);
        quote.col4 = window.app.rnd(-5, 10);

        quotes.push(quote);

        window.app.parseData(quotes);

        for (quote in window.app.series) {
            if (window.app.series.hasOwnProperty(quote)) {
                quoteCount += 1;
            }
        }

        expect(quoteCount).toBeGreaterThan(0);
    });

});
