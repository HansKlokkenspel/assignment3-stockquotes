/*jslint browser: true*/
/*global io, console, data */
(function () {
    "use strict";
    window.app = {

        headers: ['Company', 'Change', 'Date', 'Time', 'Something', 'Something', 'Something', 'Something'],

        series: {},

        socket: io('http://localhost:1337'),

        dataType: 'xhr',

        /**
         * Global app settings regarding retrieving data
         */
        settings: {
            refresh: 5000,
            ajaxUrl: 'https://query.yahooapis.com/v1/public/yql?q=select%20Change%2C%20Symbol%2C%20Name%20from%20yahoo.finance.quotes%20where%20symbol%20in(%22BCS%22%2C%22STT%22%2C%22JPM%22%2C%22LGEN.L%22%2C%22UBS%22%2C%22DB%22%2C%22BEN%22%2C%22CS%22%2C%22BK%22%2C%22KN.PA%22%2C%22GS%22%2C%22LM%22%2C%22MS%22%2C%22MTU%22%2C%22NTRS%22%2C%22GLE.PA%22%2C%22BAC%22%2C%22AV%22%2C%22SDR.L%22%2C%22DODGX%22%2C%22SLF%22%2C%22SL.L%22%2C%22NMR%22%2C%22ING%22%2C%22BNP.PA%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=',
            dataPoints: 100
        },

        rnd: function (input, range) {
            var max = input + range,
                min = input - range;
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },

        /**
         * Get a date in the format of m/d/y
         *
         * @param d, date to parse
         * @returns {string|*}
         */
        getFormattedDate: function (d) {
            var mm, dd, yyyy;

            yyyy = d.getFullYear().toString();
            mm = (d.getMonth() + 1).toString();
            dd = d.getDate().toString();
            return (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + '/' + yyyy;
        },

        /**
         * Get time in the format of am/pm
         *
         * @param d, time to format
         * @returns {*}
         */
        getFormattedTime: function (d) {
            var minutes, hours, time;

            hours = d.getHours().toFixed();
            minutes = d.getMinutes().toString();

            if (hours.length > 1) {
                if (minutes.length < 2) {
                    minutes = "0" + minutes.toString();
                }
                time = (24 - hours).toString() + ":" + minutes + " pm";
            } else {
                time = hours.toString() + ":" + minutes + " am";
            }

            return time;
        },

        /**
         * Add quote data to global quote object
         *
         * @param quote, quote to add to the global quotes
         */
        addToSeries: function (quote) {
            window.app.series[quote.col0] = quote;
        },

        /**
         * Generate some testdata in the format of the quotes we will be getting
         */
        generateTestData: function () {
            var company, quote, newQuote, date = new Date();

            for (company in window.app.series) {
                if (window.app.series.hasOwnProperty(company)) {
                    quote = window.app.series[company];
                    newQuote = Object.create(quote);
                    newQuote.col0 = company;
                    newQuote.col1 = window.app.rnd(0, 100);
                    newQuote.col2 = window.app.getFormattedDate(date); // new date
                    newQuote.col3 = window.app.getFormattedTime(date); // new time, including am, pm
                    newQuote.col4 = window.app.rnd(-5, 10); // difference of price value between this quote and the previous quote
                    newQuote.col5 = quote.col5;
                    newQuote.col6 = quote.col6;
                    newQuote.col7 = quote.col7;
                    newQuote.col8 = window.app.rnd(0, 100000);

                    window.app.addToSeries(newQuote);
                }
            }
        },

        /**
         * Parses data into the window.app.series object
         *
         * @param rows, data to parse
         */
        parseData: function (rows) {
            var i;
            // Iterate over the rows and add to series
            for (i = 0; i < rows.length; i += 1) {
                window.app.addToSeries(rows[i]);
            }
        },

        /**
         * retrieves data using an XMLHttpRequest, per https://developer.mozilla.org/en/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest
         */
        retrieveXHR: function () {
            var oReq = new XMLHttpRequest();
            oReq.addEventListener("load", window.app.JSONListener);
            oReq.open("GET", window.app.settings.ajaxUrl);
            oReq.send();
        },

        /**
         * Listener function to handle the response of the XHR
         *
         * @param res, response
         * @constructor
         */
        JSONListener: function (res) {
            var jsonData = JSON.parse(res.target.responseText), formattedData = [], quote, i = 0, date;

            quote = jsonData.query.results.quote;

            for (i; i < quote.length; i += 1) {
                date = new Date();
                formattedData[i] = {};
                formattedData[i].col0 = quote[i].Symbol;
                formattedData[i].col1 = window.app.rnd(0, 100);
                formattedData[i].col2 = window.app.getFormattedDate(date);
                formattedData[i].col3 = window.app.getFormattedTime(date);
                formattedData[i].col4 = quote[i].Change;
                formattedData[i].col5 = NaN;
                formattedData[i].col6 = NaN;
                formattedData[i].col7 = NaN;
                formattedData[i].col8 = window.app.rnd(0, 100000);
            }

            window.app.parseData(formattedData);
        },

        /**
         * Creates a valid css class attribute name for the passed in string
         *
         * @param str, future class name
         * @returns {string|XML|void}
         */
        createValidCSSNameFromCompany: function (str) {
            // regular expression to remove everything
            // that is not out of A-z0-9
            return str.replace(/\W/g, "");
        },

        /**
         * Generate HTML elements to show the window.app.series data in
         *
         * @returns {Element|*} DOM Table element
         */
        showData: function () {
            var table, tableHead, tableBody, theadRow, company, row, quote, cell, propertyName, propertyValue, i = 0, th;

            // Create table
            table = document.createElement("table");
            tableHead = document.createElement("thead");
            tableBody = document.createElement("tbody");

            theadRow = document.createElement("tr");

            // Build the 'th' element
            for (i; i < window.app.headers.length; i += 1) {
                th = document.createElement("th");
                th.innerHTML = window.app.headers[i];
                theadRow.appendChild(th);
            }

            tableHead.appendChild(theadRow);

            // Create rows
            for (company in window.app.series) {
                if (window.app.series.hasOwnProperty(company)) {
                    quote = window.app.series[company];
                    row = document.createElement("tr");
                    row.id = window.app.createValidCSSNameFromCompany(company);

                    // Create cells
                    tableBody.appendChild(row);

                    // Iterate over quote to create cells
                    for (propertyName in quote) {
                        if (quote.hasOwnProperty(propertyName)) {
                            propertyValue = quote[propertyName];
                            cell = document.createElement("td");
                            cell.innerText = propertyValue;
                            row.appendChild(cell);
                        }
                    }

                    if (quote.col4 < 0) {
                        row.className = "loser";
                    } else if (quote.col4 > 0) {
                        row.className = "winner";
                    }
                }
            }

            table.appendChild(tableHead);
            table.appendChild(tableBody);

            return table;
        },

        /**
         * Get data with sockets
         */
        getRealtimeData: function () {
            window.app.socket.on('stockquotes', function (data) {
                window.app.parseData(data.query.results.row);
            });
        },

        /**
         * The retrieve quotes loop, this will generate data if necessary, and updates the page with new data.
         */
        loop: function () {
            var table, typeHeader;

            if (window.app.dataType === 'gen') {
                window.app.generateTestData();
                window.app.dataType = 'xhr';
            } else if (window.app.dataType === 'realtime') {
                window.app.getRealtimeData();
                window.app.dataType = 'xhr';
            } else {
                window.app.retrieveXHR();
                window.app.dataType = 'gen';
            }

            typeHeader = document.createElement("h2");
            typeHeader.innerHTML = window.app.dataType;

            // remove old table
            document.querySelector("#container").removeChild(document.querySelector("h2"));
            document.querySelector("#container").removeChild(document.querySelector("table"));
            document.querySelector("#container").appendChild(typeHeader);

            // add new table
            table = window.app.showData();
            window.app.container.appendChild(table);

            setTimeout(window.app.loop, window.app.settings.refresh);
        },

        /**
         * Initializes the HTML in order to update the old container elements with the new ones.
         * Elements get updated by removal and then redrawn, which needs an initial element to delete.
         *
         * @returns {Element|*}
         */
        initHTML: function () {
            var container, h1Node, h2Node;

            // Create container
            container = document.createElement("div");
            container.id = "container";

            window.app.container = container;

            // Create title of application
            h1Node = document.createElement("h1");
            h1Node.innerText = "Real Time Stockquote App";

            h2Node = document.createElement("h2");
            h2Node.innerHTML = "init data";

            window.app.container.appendChild(h1Node);
            window.app.container.appendChild(h2Node);

            return window.app.container;
        },

        /**
         * Main initializer function, calls the respective data-retrieving functions and initializes HTML.
         * Sets up a loop for the application to run continuously.
         */
        init: function () {
            var container, table;

            // Add HTML to page
            container = window.app.initHTML();
            document.querySelector("body").appendChild(container);


            // Parse initial data
            window.app.parseData(data.query.results.row);

            table = window.app.showData();
            window.app.container.appendChild(table);

            window.app.loop();
        }
    };
}());