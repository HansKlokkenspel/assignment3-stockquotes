Document all concepts and your implementation decisions.

---


#Return values from yahoo
Values available in the test urls.

    s   Symbol
    l1  Last Trade (Price Only)
    d1  Date of Last Trade
    t1  Time of Last Trade
    c1  Change (in points)
    o   Open price
    h   Day’s High
    g   Day’s Low
    v   Volume

All return values

    a	Ask
    a2	Average Daily Volume
    a5	Ask Size
    b	Bid
    b2	Ask (Real-time)
    b3	Bid (Real-time)
    b4	Book Value
    b6	Bid Size
    c	Change and Percent Change
    c1	Change
    c3	Commission
    c6	Change (Real-time)
    c8	After Hours Change (Real-time)
    d	Dividend/Share
    d1	Last Trade Date
    d2	Trade Date
    e	Earnings/Share
    e1	Error Indication (returned for symbol changed / invalid)
    e7	EPS Est. Current Yr
    e8	EPS Estimate Next Year
    e9	EPS Est. Next Quarter
    f6	Float Shares
    g	Day’s Low
    g1	Holdings Gain Percent
    g3	Annualized Gain
    g4	Holdings Gain
    g5	Holdings Gain Percent (Real-time)
    g6	Holdings Gain (Real-time)
    h	Day’s High
    i	More Info
    i5	Order Book (Real-time)
    j	52-week Low
    j1	Market Capitalization
    j3	Market Cap (Real-time)
    j4	EBITDA
    j5	Change From 52-week Low
    j6	Percent Change From 52-week Low
    k	52-week High
    k1	Last Trade (Real-time) With Time
    k2	Change Percent (Real-time)
    k3	Last Trade Size
    k4	Change From 52-wk High
    k5	Percent Change From 52-week High
    l	Last Trade (With Time)
    l1	Last Trade (Price Only)
    l2	High Limit
    l2	High Limit
    l3	Low Limit
    l3	Low Limit
    m	Day's Range
    m2	Day’s Range (Real-time)
    m3	50-day Moving Avg
    m4	200-day Moving Average
    m5	Change From 200-day Moving Avg
    m6	Percent Change From 200-day Moving Average
    m7	Change From 50-day Moving Avg
    m8	Percent Change From 50-day Moving Average
    n	Name
    n4	Notes
    o	Open
    p	Previous Close
    p1	Price Paid
    p2	Change in Percent
    p5	Price/Sales
    p6	Price/Book
    q	Ex-Dividend Date
    q	Ex-Dividend Date
    r	P/E Ratio
    r1	Dividend Pay Date
    r2	P/E (Real-time)
    r5	PEG Ratio
    r6	Price/EPS Est. Current Yr
    r7	Price/EPS Estimate Next Year
    s	Symbol
    s1	Shares Owned
    s7	Short Ratio
    s7	Short Ratio
    t1	Last Trade Time
    t6	Trade Links
    t7	Ticker Trend
    t8	1 yr Target Price
    v	Volume
    v1	Holdings Value
    v7	Holdings Value (Real-time)
    v7	Holdings Value (Real-time)
    w	52-week Range
    w	52-week Range
    w1	Day's Value Change
    w1	Day’s Value Change
    w4	Day's Value Change (Real-time)
    w4	Day’s Value Change (Real-time)
    x	Stock Exchange
    x	Stock Exchange
    y	Dividend Yield
    y	Dividend Yield

### Get the company name with a ticker symbol
    http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=TICKER_SYMBOL&callback=YAHOO.Finance.SymbolSuggest.ssCallback

Replace `TICKER_SYMBOL` with your ticket symbol.


#Flow of the program

The application consists of the following method structure:

An init function, to initialize the application. 
An InitHTML function to initialize the HTML elements to be drawn on screen. 
A function to create HTML from newly retrieved data.
A loop construction which retrieves new data and refreshes the HTML.
three constructions to retrieve data with. (XHR, Sockets, POJO fake data)
A construction to parse data to usable objects for the HTML function.

The flow of the program is as follows:

1. The init function is called, this function kicks off the InitHTML function, along with parsing the initially provided data and kicks off the generate HTML table function. Lastly it calls the loop function to kick off the main application loop so new quotes can be retrieved within a specified amount of time.
1. Within the loop function, methods are being called to retrieve new Quote data. This can be through one of the three methods, which are called in an alternating fashion.
1. Next up, the newly gathered data is drawn. The HTML function takes the data added toi a global object and parses the elements to usable HTML elements and returns these. Within the loop, an update on the DOM is called.
1. Rinse and repeat until the application is broken off.

#Concepts
For every concept the following items:

- short description
- code example
- reference to mandatory documentation
- reference to alternative documentation (authoritative and authentic)

###Objects, including object creation and inheritance

The programming paradigm used in this application is object oriented programming. The paradigm in javascript is implemented in an alternative way, compared to languages like C++, C# or Java, as javascript is a prototypical language.

All object oriented languages need to be able to deal with several concepts:

1. encapsulation of data along with associated operations on the data, variously known as data members and member functions, or as data and methods, among other things.
1. inheritance, the ability to say that these objects are just like that other set of objects EXCEPT for these changes
1. polymorphism ("many shapes") in which an object decides for itself what methods are to be run, so that you can depend on the language to route your requests correctly.

OO languages wanted to be able to use static type checking, so we got the notion of a fixed class set at compile time. In the open-class version, you had more flexibility; 
in the newer version, you had the ability to check some kinds of correctness at compile time that would otherwise have required testing.

In a "class-based" language, that copying happens at compile time. In a prototype language, the operations are stored in the prototype data structure, which is copied and modified at run time. 
Abstractly, though, a class is still the equivalence class of all objects that share the same state space and methods. When you add a method to the prototype, you're effectively making an element of a new equivalence class.

```javascript
// Define the Person constructor
var Person = function(firstName) {
  this.firstName = firstName;
};

// Add a couple of methods to Person.prototype
Person.prototype.walk = function(){
  console.log("I am walking!");
};

Person.prototype.sayHello = function(){
  console.log("Hello, I'm " + this.firstName);
};

// Define the Student constructor
function Student(firstName, subject) {
  // Call the parent constructor, making sure (using Function#call)
  // that "this" is set correctly during the call
  Person.call(this, firstName);

  // Initialize our Student-specific properties
  this.subject = subject;
};

// Create a Student.prototype object that inherits from Person.prototype.
// Note: A common error here is to use "new Person()" to create the
// Student.prototype. That's incorrect for several reasons, not least 
// that we don't have anything to give Person for the "firstName" 
// argument. The correct place to call Person is above, where we call 
// it from Student.
Student.prototype = Object.create(Person.prototype); // See note below

// Set the "constructor" property to refer to Student
Student.prototype.constructor = Student;

// Replace the "sayHello" method
Student.prototype.sayHello = function(){
  console.log("Hello, I'm " + this.firstName + ". I'm studying "
              + this.subject + ".");
};

// Add a "sayGoodBye" method
Student.prototype.sayGoodBye = function(){
  console.log("Goodbye!");
};

// Example usage:
var student1 = new Student("Janet", "Applied Physics");
student1.sayHello();   // "Hello, I'm Janet. I'm studying Applied Physics."
student1.walk();       // "I am walking!"
student1.sayGoodBye(); // "Goodbye!"

// Check that instanceof works correctly
console.log(student1 instanceof Person);  // true 
console.log(student1 instanceof Student); // true
```

Source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript

###websockets

WebSockets is an advanced technology that makes it possible to open an interactive communication session between the user's browser and a server. 
With this API, you can send messages to a server and receive event-driven responses without having to poll the server for a reply.

Client side:

```javascript
// This simple example creates a new WebSocket, connecting to the server at ws://www.example.com/socketserver. 
// A custom protocol of "protocolOne" is named in the request for the socket in this example, though this can be omitted.
var exampleSocket = new WebSocket("ws://www.example.com/socketserver", "protocolOne");

// As establishing a connection is asynchronous and prone to failure there is no guarantee that calling the send() method immediately after creating a WebSocket object will be successful. 
// We can at least be sure that attempting to send data only takes place once a connection is established by defining an onopen handler to do the work:
exampleSocket.onopen = function (event) {
// Once you've opened your connection, you can begin transmitting data to the server. To do this, simply call the WebSocket object's send() method for each message you want to send:
  exampleSocket.send("Here's some text that the server is urgently awaiting!"); 
};
```

Server side:

```javascript
// The same constructs as on the client side apply, once the onmessage event has been called, we can do whatever we want with the data we receive withing the callback.
exampleSocket.onmessage = function (event) {
  console.log(event.data);
}
```

Source: 

https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API
https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_client_applications

###XMLHttpRequest

XMLHttpRequest is an API that provides client functionality for transferring data between a client and a server. It provides an easy way to retrieve data from a URL without having to do a full page refresh. 
This enables a Web page to update just a part of the page without disrupting what the user is doing.  XMLHttpRequest is used heavily in AJAX programming.

Source: https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest

####AJAX

AJAX stands for Asynchronous JavaScript and XML. In a nutshell, it is the use of the XMLHttpRequest object to communicate with server-side scripts. It can send as well as receive information in a variety of formats, including JSON, XML, HTML, and even text files. 
AJAX’s most appealing characteristic, however, is its "asynchronous" nature, which means it can do all of this without having to refresh the page. This lets you update portions of a page based upon user events.

![ajax] (https://www.google.nl/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiogL3EyJTKAhWGtA4KHRDeALgQjRwIBw&url=https%3A%2F%2Fsureshjain.wordpress.com%2Fcategory%2Fajax%2F&psig=AFQjCNExGd37x_tB9ih0xHpsZxfzNTBpMA&ust=1452148411309311)

```
<span id="ajaxButton" style="cursor: pointer; text-decoration: underline">
  Make a request
</span>
<script type="text/javascript">
(function() {
  var httpRequest;
  document.getElementById("ajaxButton").onclick = function() { makeRequest('test.html'); };

  function makeRequest(url) {
    httpRequest = new XMLHttpRequest();

    if (!httpRequest) {
      alert('Giving up :( Cannot create an XMLHTTP instance');
      return false;
    }
    httpRequest.onreadystatechange = alertContents;
    httpRequest.open('GET', url);
    httpRequest.send();
  }

  function alertContents() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        alert(httpRequest.responseText);
      } else {
        alert('There was a problem with the request.');
      }
    }
  }
})();
</script>
```

Source: https://developer.mozilla.org/en-US/docs/AJAX/Getting_Started

###Callbacks
A Callback is a function that can be passed around as a pointer to other functions. They act as a mechanism to execute a certain piece of code, when some other code has finished processing (and thus calls the passed in callback).
This is needed for javascript's asynchronous nature, and is especially handy in case of say an XHR. Once data has come back from the designated source, the callback can be executed and the data can be processed.
The callback is needed so other parts of the program can continue doing their business, and not have to wait on the XHR.

###How to write testable code for unit-tests

Writing testable code means giving a good structure to your program, and ultimately following a few principle desgin patterns:

Low coupling & high cohesion, whenever your class is dependent on some other class, don't literally couple the classes, but rather use abstraction mechanisms like interfaces.
Coupled with interfaces and class dependence, usually constructor injection is implemented to resolve these dependencies. Construction of classes is usually handled by a single class, or 'factory' if you will.
The guideline for writing testable code is to let each class or function handle a specific role, don't make Jacks of all trade.

